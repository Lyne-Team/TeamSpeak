package fr.lyneteam.nico.teamspeak;

import java.lang.reflect.Method;

public class TeamSpeakEventClassMethod {
	private final TeamSpeakEventListener listener;
	private final Class<? extends TeamSpeakEventListener> object;
	private final Method method;

	public TeamSpeakEventClassMethod(TeamSpeakEventListener listener, Class<? extends TeamSpeakEventListener> object, Method method) {
		this.listener = listener;
		this.object = object;
		this.method = method;
	}

	public final TeamSpeakEventListener getListener() {
		return this.listener;
	}

	public final Class<? extends TeamSpeakEventListener> getObject() {
		return this.object;
	}

	public final Method getMethod() {
		return this.method;
	}
}