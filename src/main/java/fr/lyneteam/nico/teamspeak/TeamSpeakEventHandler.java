package fr.lyneteam.nico.teamspeak;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) 
public @interface TeamSpeakEventHandler {	
	public enum Priority {
	   LOWEST,
	   LOW,
	   MEDIUM,
	   HIGH,
	   HIGHEST
	}

	Priority priority() default Priority.MEDIUM;
}