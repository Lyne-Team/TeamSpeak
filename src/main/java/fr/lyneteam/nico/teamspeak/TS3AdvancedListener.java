package fr.lyneteam.nico.teamspeak;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.theholywaffle.teamspeak3.api.event.BaseEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelCreateEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDeletedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDescriptionEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelPasswordChangedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientLeaveEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.PrivilegeKeyUsedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ServerEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3Listener;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;

public class TS3AdvancedListener implements TS3Listener {
	private final List<TeamSpeakEventListener> listeners;
	
	public TS3AdvancedListener(TeamSpeakEventListener... listeners) {
		this.listeners = new ArrayList<TeamSpeakEventListener>();
		this.addEventListeners(listeners);
	}
	
	public void addEventListeners(TeamSpeakEventListener... listeners) {
		for (TeamSpeakEventListener listener : listeners) this.listeners.add(listener);
	}
	
	public void removeEventListeners(TeamSpeakEventListener... listeners) {
		for (TeamSpeakEventListener listener : listeners) this.listeners.remove(listener);
	}
	
	public void callEvent(BaseEvent event) {
		try {
			if (!(event == null)) {
				try {
					Map<TeamSpeakEventHandler.Priority, List<TeamSpeakEventClassMethod>> methods = new HashMap<TeamSpeakEventHandler.Priority, List<TeamSpeakEventClassMethod>>();
					for (TeamSpeakEventHandler.Priority priority : TeamSpeakEventHandler.Priority.values()) methods.put(priority, new ArrayList<TeamSpeakEventClassMethod>());
					for (TeamSpeakEventListener listener : new ArrayList<TeamSpeakEventListener>(this.listeners)) {
						Class<? extends TeamSpeakEventListener> object = listener.getClass();
						for (Method method : object.getDeclaredMethods()) if (method.isAnnotationPresent(TeamSpeakEventHandler.class)) {
							Annotation annotation = method.getAnnotation(TeamSpeakEventHandler.class);
							TeamSpeakEventHandler handler = (TeamSpeakEventHandler) annotation;
							if (method.getParameterTypes().length == 1 && method.getParameterTypes()[0].getName().equals(event.getClass().getName())) methods.get(handler.priority()).add(new TeamSpeakEventClassMethod(listener, object, method));
						}
					}
					for (String priority : "HIGHEST HIGH MEDIUM LOW LOWEST".split(" ")) for (TeamSpeakEventClassMethod method : methods.get(TeamSpeakEventHandler.Priority.valueOf(priority))) try {
						Method cast = method.getMethod();
						cast.setAccessible(true);
						cast.invoke(method.getListener(), event);
					} catch (Exception exception) {
						System.err.println("Error sending event to a listener (" + method.getListener().getClass().getCanonicalName() + " : " + method.getMethod().getName() + ")");
						exception.printStackTrace();
					}
				} catch (Exception exception) {
					System.err.println("Error dispaching events.");
					exception.printStackTrace();
				}
			}
		} catch (Exception exception) {
			System.err.println("Error durring listening event.");
			exception.printStackTrace();
		}
	}

	public void onChannelCreate(ChannelCreateEvent event) {
		this.callEvent(event);
	}

	public void onChannelDeleted(ChannelDeletedEvent event) {
		this.callEvent(event);
	}

	public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent event) {
		this.callEvent(event);
	}

	public void onChannelEdit(ChannelEditedEvent event) {
		this.callEvent(event);
	}

	public void onChannelMoved(ChannelMovedEvent event) {
		this.callEvent(event);
	}

	public void onChannelPasswordChanged(ChannelPasswordChangedEvent event) {
		this.callEvent(event);
	}

	public void onClientJoin(ClientJoinEvent event) {
		this.callEvent(event);
	}

	public void onClientLeave(ClientLeaveEvent event) {
		this.callEvent(event);
	}

	public void onClientMoved(ClientMovedEvent event) {
		this.callEvent(event);
	}

	public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent event) {
		this.callEvent(event);
	}

	public void onServerEdit(ServerEditedEvent event) {
		this.callEvent(event);
	}

	public void onTextMessage(TextMessageEvent event) {
		this.callEvent(event);
	}
}